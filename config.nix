{
  allowUnfree = true;
  chromium = {
    enablePepperFlash = true;
    enablePepperPDF = true;
    enableWideVine = true;
  };

  packageOverrides = super: let self = super.pkgs; in

  rec {
    jsonnet = super.jsonnet.overrideAttrs (oldAttrs: {
      name = "jsonnet-0.9.4";
      src = super.fetchFromGitHub {
        rev = "v0.9.4";
      	owner = "google";
      	repo = "jsonnet";
      	sha256 = "1bh9x8d3mxnic31b6gh4drn5l6qpyqfgsn2l48sv0jknhinm1a7l";
      };
    });
    emacsPackages = self.emacsPackagesNgGen self.emacs;
    emacs-env = emacsPackages.emacsWithPackages (epkgs: with epkgs.melpaPackages; [
      idris-mode
      go-mode
      solarized-theme
      dracula-theme
      popup-imenu
      go-autocomplete
      go-rename
      nix-mode
      magit
      projectile
      powerline
      use-package
      company
      helm
      helm-swoop
      helm-projectile
      elpy
      function-args
      jedi
      yapfify
      clojure-mode
      cider
      yaml-mode
      haskell-mode
      ghc
      hindent
    ]);
  };
}
