# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  virtualisation.docker.enable = true;
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };
  hardware.bluetooth.enable = true;
  hardware.opengl.driSupport32Bit = true;
  # services.xserver.windowManager.exwm = {
  #   enable = true;
  #   extraPackages = epkgs: [
  #     epkgs.idris-mode
  #     epkgs.go-mode
  #     epkgs.solarized-theme
  #     epkgs.dracula-theme
  #     epkgs.popup-imenu
  #     epkgs.go-autocomplete
  #     epkgs.go-rename
  #     epkgs.nix-mode
  #     epkgs.magit
  #     epkgs.projectile
  #     epkgs.powerline
  #     epkgs.use-package
  #     epkgs.company
  #     epkgs.helm
  #     epkgs.helm-swoop
  #     epkgs.helm-projectile
  #     epkgs.elpy
  #     epkgs.yapfify
  #     epkgs.clojure-mode
  #     epkgs.cider
  #     epkgs.yaml-mode
  #   ];
  # };

  users.extraUsers.shimin = {
    isNormalUser = true;
    home = "/home/shimin";
    extraGroups = [ "wheel" "networkmanager" "docker" ];
    shell = "/run/current-system/sw/bin/zsh";
  };
  nixpkgs.config = {
    chromium = {
      enablePepperFlash = true;
      enablePepperPDF = true;
      enableWideVine = true;
    };
  };
  services.xserver.xrandrHeads = ["eDP1" "DP1"];

  networking.vpnc = {
    services = {
      vpn = ''
        IPSec gateway mp-czhttzqwvd.dynamic-m.com
        IPSec secret K5kpyG691yh6KSZAG9SK6VtTZoTfiMZo
        Xauth username sm_588282701325488184@meraki.com
        Xauth password MMKmYqACDGL5Xm7P4fU4bNtQpie9Ed9XTl
      '';
    };
  };
  systemd.services.network-manager.path = [pkgs.kmod];
  sound.mediaKeys.enable = true;
  services.printing.enable = true;
  services.avahi.enable = true;
  nixpkgs.config.allowUnfree = true;
  services.printing.drivers = [ pkgs.splix pkgs.canon-cups-ufr2 ];

  networking.networkmanager.enable = true;
  networking.extraHosts = ''
    169.53.131.184 shimin
    96.114.36.103 wifilogin.xfinity.com
    172.31.98.1 aruba.odyssys.net
  '';
  # networking.wireless.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };
  i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
    inputMethod = {
      enabled = "fcitx";
      fcitx.engines = with pkgs.fcitx-engines; [
        cloudpinyin
      ];
    };
  };

  services.resolved = {
    enable = false;
    domains = ["corp.mixpanel.com."];
    fallbackDns = ["8.8.8.8" "8.8.4.4"];
  };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    wqy_microhei
    wqy_zenhei
  ];

  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      wqy_zenhei
      wqy_microhei
    ];
  };

  programs.zsh.enable = true;

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  # environment.systemPackages = with pkgs; [
  #   wget
  # ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.xkbOptions = "ctrl:nocaps";
  # services.xserver.windowManager.xmonad.enable = true;
  # services.xserver.windowManager.xmonad.enableContribAndExtras = true;
  # services.xserver.windowManager.default = "xmonad";
  services.xserver.synaptics = {
    enable = true;
    twoFingerScroll = true;
  };

  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  # services.xserver.desktopManager.gnome3.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.extraUsers.guest = {
  #   isNormalUser = true;
  #   uid = 1000;
  # };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "17.03";

  services.udev.extraRules = ''
    # do not edit this file, it will be overwritten on update

    ACTION!="add", GOTO="graphics_end"

    # Tag the drm device for KMS-supporting drivers as the primary device for
    # the display; for non-KMS drivers tag the framebuffer device instead.

    SUBSYSTEM!="drm", GOTO="drm_end"
    KERNEL!="card[0-9]*", GOTO="drm_end"
    ENV{DEVTYPE}!="drm_minor", GOTO="drm_end"

    DRIVERS=="i915", ENV{PRIMARY_DEVICE_FOR_DISPLAY}="1"
    DRIVERS=="radeon", ENV{PRIMARY_DEVICE_FOR_DISPLAY}="1"
    DRIVERS=="nouveau", ENV{PRIMARY_DEVICE_FOR_DISPLAY}="1"

    LABEL="drm_end"

    SUBSYSTEM!="graphics", GOTO="graphics_end"

    DRIVERS=="i915", GOTO="graphics_end"
    DRIVERS=="radeon", GOTO="graphics_end"
    DRIVERS=="nouveau", GOTO="graphics_end"

    KERNEL=="fb[0-9]*", ENV{PRIMARY_DEVICE_FOR_DISPLAY}="1"

    LABEL="graphics_end"
  '';
  networking.extraResolvconfConf = ''
    name_servers=8.8.8.8
    search_domains=corp.mixpanel.com
  '';
}
